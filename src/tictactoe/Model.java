/**
 * Die Klasse "Model" enthält die gesamte Spiellogik bzw. die Funktionsweise und Kodierung des Spiels.
 *
 * @author Michael Berger
 */

package tictactoe;

public class Model{

    private int spielfeld;

    /**
     * Wertet aus ob bereits ein Gewinner feststeht oder nicht.
     *
     * @return: Gibt jeweils true zurück, falls ein Gewinner feststeht und false, falls kein Gewinner feststeht.
     */

    public boolean istGewonnen(){

        int merkerX = 0, merkerO = 0;

        //1. Zeile

        for (int i = 0; i < 6; i = i + 2) {

            boolean istO = ((spielfeld >> i) & 1) == 1;
            boolean istX = ((spielfeld >> i + 1) & 1) == 1;

            if (!istO & istX) {
                merkerX++;
            }

            if (istO & !istX) {
                merkerO++;
            }
        }

        if (merkerX == 3 | merkerO == 3)return true;

        merkerX = 0; merkerO = 0;

        //2. Zeile

        for (int i = 6; i < 12; i = i + 2) {

            boolean istO = ((spielfeld >> i) & 1) == 1;
            boolean istX = ((spielfeld >> i + 1) & 1) == 1;

            if (!istO & istX) {
                merkerX++;
            }

            if (istO & !istX) {
                merkerO++;
            }
        }

        if (merkerX == 3 | merkerO == 3)return true;

        merkerX = 0; merkerO = 0;

        //3. Zeile

        for (int i = 12; i < 18; i = i + 2) {

            boolean istO = ((spielfeld >> i) & 1) == 1;
            boolean istX = ((spielfeld >> i + 1) & 1) == 1;

            if (!istO & istX) {
                merkerX++;
            }

            if (istO & !istX) {
                merkerO++;
            }
        }

        if (merkerX == 3 | merkerO == 3)return true;

        merkerX = 0; merkerO = 0;

        //1. Spalte

        for (int i = 0; i < 14; i = i + 6) {

            boolean istO = ((spielfeld >> i) & 1) == 1;
            boolean istX = ((spielfeld >> i + 1) & 1) == 1;

            if (!istO & istX) {
                merkerX++;
            }

            if (istO & !istX) {
                merkerO++;
            }
        }

        if (merkerX == 3 | merkerO == 3)return true;

        merkerX = 0; merkerO = 0;

        //2. Spalte

        for (int i = 2; i < 16; i = i + 6) {

            boolean istO = ((spielfeld >> i) & 1) == 1;
            boolean istX = ((spielfeld >> i + 1) & 1) == 1;

            if (!istO & istX) {
                merkerX++;
            }

            if (istO & !istX) {
                merkerO++;
            }
        }

        if (merkerX == 3 | merkerO == 3)return true;

        merkerX = 0; merkerO = 0;

        //3. Spalte

        for (int i = 4; i < 18; i = i + 6) {

            boolean istO = ((spielfeld >> i) & 1) == 1;
            boolean istX = ((spielfeld >> i + 1) & 1) == 1;

            if (!istO & istX) {
                merkerX++;
            }

            if (istO & !istX) {
                merkerO++;
            }
        }

        if (merkerX == 3)return true;
        if (merkerO == 3)return true;

        merkerX = 0; merkerO = 0;

        //1. Diagonale

        for (int i = 0; i < 18; i = i+2) {

            if(i==0 | i==8 | i==16){

                boolean istO = ((spielfeld >> i) & 1) == 1;
                boolean istX = ((spielfeld >> i + 1) & 1) == 1;

                if (!istO & istX) {
                    merkerX++;
                }

                if (istO & !istX) {
                    merkerO++;
                }
            }
        }

        if (merkerX == 3 | merkerO == 3)return true;

        merkerX = 0; merkerO = 0;

        //2. Diagonale

        for (int i = 0; i < 18; i = i+2) {

            if(i==4| i==8 | i==12){

                boolean istO = ((spielfeld >> i) & 1) == 1;
                boolean istX = ((spielfeld >> i + 1) & 1) == 1;

                if (!istO & istX) {
                    merkerX++;
                }
                if (istO & !istX) {
                    merkerO++;
                }
            }
        }

        if (merkerX == 3 | merkerO == 3) return true;

        else return false;
    }

    /**
     * Wertet aus ob ein Spiel unentschieden ausgegangen ist.
     *
     * @return: Gibt true zurück, falls das Spiel unentschieden ausging und gibt andernfalls false zurück.
     */
    public boolean istUnentschieden() {

        int merker = 0;
        for (int i = 0; i < 18; i = i + 2) {

            boolean istBelegt = (((spielfeld >> i) & 1) | ((spielfeld >> i + 1) & 1)) == 1;


            if (istBelegt) {
                merker++;
            }
        }

        if (merker == 9 & !istGewonnen())
            return true;
        else
            return false;
    }

    /**
     * Je nachdem welche Eingabe getätigt wurde, werden hier die Bits im spielfeld-Integer gesetzt, welche das TicTacToe feld verkörpern.
     *
     * @param zeile: Steht für die entsprechende Zeile die angegeben wurde.
     * @param spalte: Steht für die entsprechende Spalte die angegeben wurde.
     * @param spieler: Der Spieler der momentan am Zug ist (X oder O).
     * @return: Gibt den spielfeld-Integer mit den neu gesetzten Bits zurück.
     */
    public int setFelder(int zeile, int spalte, int spieler) {

        if (zeile == 3 & spalte == 3 & istZugErlaubt(zeile, spalte)) {
            if (spieler == 0b01) {
                spielfeld = spielfeld | 0b01_00_00_00_00_00_00_00_00;
            }
            if (spieler == 0b10) {
                spielfeld = spielfeld | 0b10_00_00_00_00_00_00_00_00;
            }
        }

        if (zeile == 3 & spalte == 2 & istZugErlaubt(zeile, spalte)) {
            if (spieler == 0b01) {
                spielfeld = spielfeld | 0b00_01_00_00_00_00_00_00_00;
            }
            if (spieler == 0b10) {
                spielfeld = spielfeld | 0b00_10_00_00_00_00_00_00_00;
            }
        }

        if (zeile == 3 & spalte == 1 & istZugErlaubt(zeile, spalte)) {
            if (spieler == 0b01) {
                spielfeld = spielfeld | 0b00_00_01_00_00_00_00_00_00;
            }

            if (spieler == 0b10) {
                spielfeld = spielfeld | 0b00_00_10_00_00_00_00_00_00;
            }
        }

        if (zeile == 2 & spalte == 3 & istZugErlaubt(zeile, spalte)) {
            if (spieler == 0b01) {
                spielfeld = spielfeld | 0b00_00_00_01_00_00_00_00_00;
            }

            if (spieler == 0b10) {
                spielfeld = spielfeld | 0b00_00_00_10_00_00_00_00_00;
            }
        }

        if (zeile == 2 & spalte == 2 & istZugErlaubt(zeile, spalte)) {
            if (spieler == 0b01) {
                spielfeld = spielfeld | 0b00_00_00_00_01_00_00_00_00;
            }

            if (spieler == 0b10) {
                spielfeld = spielfeld | 0b00_00_00_00_10_00_00_00_00;
            }
        }

        if (zeile == 2 & spalte == 1 & istZugErlaubt(zeile, spalte)) {
            if (spieler == 0b01) {
                spielfeld = spielfeld | 0b00_00_00_00_00_01_00_00_00;
            }

            if (spieler == 0b10) {
                spielfeld = spielfeld | 0b00_00_00_00_00_10_00_00_00;
            }
        }

        if (zeile == 1 & spalte == 3 & istZugErlaubt(zeile, spalte)) {
            if (spieler == 0b01) {
                spielfeld = spielfeld | 0b00_00_00_00_00_00_01_00_00;
            }

            if (spieler == 0b10) {
                spielfeld = spielfeld | 0b00_00_00_00_00_00_10_00_00;
            }
        }

        if (zeile == 1 & spalte == 2 & istZugErlaubt(zeile, spalte)) {
            if (spieler == 0b01) {
                spielfeld = spielfeld | 0b00_00_00_00_00_00_00_01_00;
            }

            if (spieler == 0b10) {
                spielfeld = spielfeld | 0b00_00_00_00_00_00_00_10_00;
            }
        }

        if (zeile == 1 & spalte == 1 & istZugErlaubt(zeile, spalte)) {
            if (spieler == 0b01) {
                spielfeld = spielfeld | 0b00_00_00_00_00_00_00_00_01;
            }

            if (spieler == 0b10) {
                spielfeld = spielfeld | 0b00_00_00_00_00_00_00_00_10;
            }
        }

        return spielfeld;
    }

    /**
     * Prüft ob die getätigte Eingabe bzw. der gewünschte Spielzug erlaubt ist oder nicht.
     *
     * @param zeile: Steht für die entsprechende Zeile die angegeben wurde.
     * @param spalte: Steht für die entsprechende Spalte die angegeben wurde.
     * @return: Gibt true zurück, falls der Zug erlaubt ist und false, falls er nicht erlaubt ist.
     */

    public boolean istZugErlaubt(int zeile, int spalte) {

        int j = 0;

        if (zeile == 1 & spalte == 1)
            j = 0;
        if (zeile == 1 & spalte == 2)
            j = 2;
        if (zeile == 1 & spalte == 3)
            j = 4;

        if (zeile == 2 & spalte == 1)
            j = 6;
        if (zeile == 2 & spalte == 2)
            j = 8;
        if (zeile == 2 & spalte == 3)
            j = 10;

        if (zeile == 3 & spalte == 1)
            j = 12;
        if (zeile == 3 & spalte == 2)
            j = 14;
        if (zeile == 3 & spalte == 3)
            j = 16;

        boolean istBelegt = (((spielfeld >> j) & 1) | ((spielfeld >> j + 1) & 1)) == 1;

        if (!istBelegt & zeile < 4 & zeile > 0 & spalte < 4 & spalte > 0) {
            return true;
        }
        else
            return false;
    }

    /**
     * Bestimmt auf Basis des aktuellen Spielzugs welcher Spieler (X oder O) am Zug ist.
     *
     * @param spielzug: Entspricht dem aktuellen Spielzug und wird benutzt um den aktuellen Spieler zu ermitteln.
     * @return: Gibt zurück welcher Spieler am Zug ist.
     */

    public int getSpieler(int spielzug){

        int spieler;

        if(spielzug % 2 == 0){
            spieler = 0b10;
        }

        else spieler = 0b01;

        return spieler;
    }
}

