/**
 * Die Klasse "View" ist für sämtliche Textausgaben wie Spielanweisungen, Spielfelder, Anzahl der Züge,.. zuständig.
 *
 * @author Michael Berger
 */

package tictactoe;

public class View {

    private Model model;
    private int zeile, spalte, spieler;


    public View(Model model) {

        this.model = model;
    }

    /**
     * Gibt die Spielanweisungen sowie zu Berginn des Spiels das Startfeld aus.
     * @param spielzug: Durch den spielzug Parameter wird die Spielreihenfolge festgelegt, außerdem steht er natürlich für den jeweils aktuellen Spielzug während des Spiels.
     */

    public void ausgabeSpielanweisung(int spielzug) {

        String spieler = "";

        if (model.getSpieler(spielzug) == 0b10)
            spieler = "X";

        if(model.getSpieler(spielzug) == 0b01)
            spieler = "O";

        if(spielzug == 0){
            System.out.println("Das Spiel hat begonnen! Spieler " + spieler + ", bitte geben Sie Ihren " + (spielzug+1) + ". Spielzug an:"
                    + "\n" + "Geben Sie jeweils eine Nummer von 1-3 für 1) die gewünschte Spalte und 2) die gewünschte Zeile ein und bestätigen Sie mit Enter." + "\n");

            System.out.print("\n"+"   |   |   "+"\n"+
                    "-----------"+"\n"+
                    "   |   |   "+"\n"+
                    "-----------"+"\n"+
                    "   |   |   "+"\n"+"\n");
        }

        else System.out.println("\n" + "Spieler " + spieler + ", bitte geben Sie den " + (spielzug+1) + ". Spielzug an:" + "\n");
    }

    /**
     * Gibt jeweils nach einer korrekten Eingabe das aktuelle Spielfeld aus.
     */

    public void ausgabeFeld(){

        for (int i = 0; i < 18; i = i + 2){

            boolean istO = ((model.setFelder(zeile, spalte, spieler) >> i) & 1) == 1;
            boolean istX = ((model.setFelder(zeile, spalte, spieler) >> i + 1) & 1) == 1;

            if ((istO & !istX & i == 0) | (istO & !istX & i == 6) | (istO & !istX & i == 12)) System.out.print(" O |");
            if ((!istO & istX & i == 0) | (!istO & istX & i == 6) | (!istO & istX & i == 12)) System.out.print(" X |");
            if ((!istO & !istX & i == 0) | (!istO & !istX & i == 6) | (!istO & !istX & i == 12)) System.out.print("   |");

            if ((istO & !istX & i == 2) | (istO & !istX & i == 8) | (istO & !istX & i == 14)) System.out.print(" O ");
            if ((!istO & istX & i == 2) | (!istO & istX & i == 8) | (!istO & istX & i == 14)) System.out.print(" X ");
            if ((!istO & !istX & i == 2) | (!istO & !istX & i == 8) | (!istO & !istX & i == 14)) System.out.print("   ");

            if ((istO & !istX & i == 4) | (istO & !istX & i == 10) | (istO & !istX & i == 16)) System.out.print("| O ");
            if ((!istO & istX & i == 4) | (!istO & istX & i == 10) | (!istO & istX & i == 16)) System.out.print("| X ");
            if ((!istO & !istX & i == 4) | (!istO & !istX & i == 10) | (!istO & !istX & i == 16)) System.out.print("|  ");


            if (i == 4 | i == 10) System.out.print("\n" + "-----------" + "\n");
            if (i == 16) System.out.print("\n" + "\n");
        }
    }

    /**
     * Ausgabe im Falle eines unentschiedenen Spiels.
     */

    public void ausgabeIstUnentschieden() {

        System.out.println("\n" + "Das Spiel endete unentschieden!");
    }

    /**
     * Gibt bei Spielende den Gewinner aus.
     *
     * @param spielzug: Wird erneut benutzt um den Spieler zu bestimmen (durch die Methode: getSpieler()).
     */

    public void ausgabeIstGewonnen(int spielzug) {

        String spieler = "";

        if (model.getSpieler(spielzug) == 0b01) // verdreht da zähler für spielstand schon eins hochgezaehlt hat
            spieler="X";

        if(model.getSpieler(spielzug) == 0b10)
            spieler="O";

        System.out.println("\n" + "Herzlichen Glückwunsch Spieler " + spieler + "! Sie haben das Spiel gewonnen!");
    }

    /**
     * Entsprechende Ausgabe, falls ein ungültiger Spielzug bzw. eine ungültige Eingabe gemacht wurde.
     *
     * @param zeile: Wird in der Methode istZugErlaubt() benutzt um die Korrektheit eines Spielzuges zu bestimmen.
     * @param spalte : Wird in der Methode istZugErlaubt() benutzt um die Korrektheit eines Spielzuges zu bestimmen.
     */

    public void ausgabeUngueltigerZug(int zeile, int spalte) {

        if (!model.istZugErlaubt(zeile, spalte)) {

            System.out.println("Ungültiger Zug oder ungültige Eingabe, bitte wiederholen: " + "\n");
        }
    }
}
