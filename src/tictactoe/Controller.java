/**
 * Durch die Klasse Controller wird das Spiel gestartet, zus�tzlich werden hier die Eingaben entegegengenommen und mit Hilfe der Klasse "Model" werden
 * die Spielregeln bzw. der Spielablauf kontrolliert.
 *
 * @author Michael Berger
 */

package tictactoe;

public class Controller {

    private int zeile, spalte, spielzug;

    private View view;
    private Model model;
    private Eingabe eingabe;


    public Controller(View view, Model model, Eingabe eingabe) {

        this.model = model;
        this.view = view;
        this.eingabe = eingabe;
    }

    /**
     * Solange das Spiel nicht unentschieden ausgeht oder der Gewinner feststeht l�uft die �u�ere Schleife in der Eingaben entgegen genommen werden
     * und dementsprechend �ber die Klasse "View" ausgegeben werden.
     * Die innere Schleife bewirkt, dass neue Felder ausschlie�lich gesetzt werden, solange die eingegebenen Z�ge erlaubt sind, und gibt bei erlaubten Z�gen
     * dementsprechend das Spielfeld aus.

     * Sobald das Spiel unentschieden oder gewonnen ausgeht, ist es zu Ende und es wird eine entsprechende Ausgabe ausgegeben.
     */
    public void start() {

        while (!model.istGewonnen() &&  !model.istUnentschieden()) {
            model.getSpieler(spielzug);
            view.ausgabeSpielanweisung(spielzug);
            spalte = eingabe.readInt();
            zeile = eingabe.readInt();
            view.ausgabeUngueltigerZug(zeile, spalte);

            while(model.istZugErlaubt(zeile, spalte)){
                model.setFelder(zeile, spalte, model.getSpieler(spielzug));
                view.ausgabeFeld();
                spielzug++;
            }
        }

        if (model.istUnentschieden()) {
            view.ausgabeIstUnentschieden();
        }

        if (model.istGewonnen()) {
            view.ausgabeIstGewonnen(spielzug);
        }
    }
}
