/**
 * Mainklasse, die die Objekte erzeugt und die Klasse "Controller" zum Spielstart aufruft.
 *
 * @author Michael Berger
 */

package tictactoe;

public class Main {

    public static void main(String[] args) {

        Eingabe eingabe = new Eingabe();
        Model model = new Model();
        View view = new View(model);
        Controller controller = new Controller(view, model, eingabe);
        controller.start();
    }
}